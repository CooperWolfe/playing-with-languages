# Playing with Flex and Bison

## Contents
main.c
 - main implementation: drives flex and bison from input file
 - includes necessary files
 - prints header
 - directs scanning/parsing of input file
 - some relatively useless code whose purpose is to silence warnings from code not implemented by me

main.in
  - flex source code: converts input into tokens:
  - character 'a' translated to token A
  - character 'c' translated to token C
  - EOF character translated once to token END (flex tends to read EOF multiple times when converted to a token)
  - all other characters translated to token INVALID which is not used by bison

main.y
  - bison source code: constructs languages from tokens:
  - Outputs language s1 membership (ac{ac}+) for entire input
  - Outputs language s2 membership (aa+c+) for entire input
  - Outputs language s3 membership (c+a+) for entire input
  - Calls yyerror for input that does not belong to any of the three languages

yyerror.c
  - error handler implementation: outputs no language membership
