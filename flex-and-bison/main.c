#include "whichclass.tab.c"
#include "lex.yy.c"
#include "yyerror.c"

void yyunputBuff(int c, register char * yy_bp) {}
int inputBuff() { return 0; }

int main() {
	printf("\n>>>> 3520 Grammatical Recognizer (SDE1) <<<<<<\n");
	yyparse();

	// silence lex.yy.c warnings
	void (*yyunputSilencer)(int, register char *) = &yyunput;
	yyunputSilencer = &yyunputBuff;
	(*yyunputSilencer)(0, "");
	int (*inputSilencer)() = &input;
	inputSilencer = &inputBuff;
	(*inputSilencer)();

	return 1;
}
