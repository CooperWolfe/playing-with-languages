%{
#include <stdio.h>
#include <ctype.h>
int yylex();
int yyerror(char *s);
%}

%token A;
%token C;
%token END;
%token INVALID;

%%

s: s1 END
   { printf("class is w1\n\n"); }
 | s2 END
   { printf("class is w2\n\n"); }
 | s3 END
   { printf("class is w3\n\n"); }
;

as: A
  | as A
;
cs: C
  | cs C
;

s1: A C A C
  | s1 A C
;

s2: as A cs
;

s3: cs as
;
