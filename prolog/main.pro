% Grammar 1
s1 --> [a], [c], d, !.
d --> s1.
d --> [a], [c].

% Grammar 2
s2 --> [a], a, c, !.
a --> [a], a.
a --> [a].
c --> [c], c.
c --> [c].

% Grammar 3
s3 --> g, h, !.
g --> [c], g.
g --> [c].
h --> [a], h.
h --> [a].

% None
none(A) :-
	not(s1(A, [])),
	not(s2(A, [])),
	not(s3(A, [])),
	!.
